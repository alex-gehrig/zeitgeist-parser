#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys
import sqlite3
import time
import csv


if len(sys.argv) < 2:
    print("Die zu verarbeitende Datenbank muss mit angegeben werden.\n"
          "Das Skript wird nun beendet.")
    sys.exit()
else:
    dbname = sys.argv[1]


conn = sqlite3.connect(dbname)
cur = conn.cursor()


timestamp = time.strftime("%A %d.%m.%Y %H-%M-%S %Z")
filename = "report_" + dbname[:-7] + "_" + timestamp + ".csv"
target = open(filename, 'w')


mywriter = csv.writer(target, delimiter=";")
mywriter.writerow(["ID", "Zeitpunkt", "Art der Aktion", "Auslöser", "Anwendung", "Payload",
                   "Datei-/Programmname mit Pfad", "Typ", "Details zum Typ", "Mimetype",
                   "Übergeordneter Pfad", "Programm-/ Datei-/ Pfadname (ggf. nach Umbenennen etc.)",
                   "Speicherwert", "Ursprung", "(neuer) Datei-/Programmname mit Pfad", "(neuer) Übergeordneter Pfad"])

# timestamp ist NOT unixtime! It's "gint64", which means milliseconds since the Unix Epoch,
# so a division by 1000 is necessary

for row in cur.execute("""
                        SELECT e.id,
                        datetime((e.timestamp/1000), 'unixepoch', 'localtime'),
                        inter.value,
                        manifest.value,
                        actor.value,
                        payload.value,
                        uri.value,
                        s_inter.value,
                        s_manifest.value,
                        mime.value,
                        s_uri.value,
                        text.value,
                        storage.value,
                        origin_uri.value,
                        s_current_uri.value,
                        o_current_uri.value
                        
                        

                        FROM event AS e
                        LEFT JOIN interpretation AS inter
                            ON e.interpretation = inter.id
                        LEFT JOIN manifestation AS manifest
                            ON e.manifestation = manifest.id
                        LEFT JOIN interpretation AS s_inter
                            ON e.subj_interpretation = s_inter.id
                        LEFT JOIN manifestation AS s_manifest
                            ON e.subj_manifestation = s_manifest.id
                        LEFT JOIN actor
                            ON e.actor = actor.id
                        LEFT JOIN mimetype AS mime
                            ON e.subj_mimetype = mime.id
                        LEFT JOIN text
                            ON e.subj_text = text.id
                        LEFT JOIN uri
                            ON e.subj_id = uri.id
                        LEFT JOIN payload
                            ON e.payload = payload.id
                        LEFT JOIN uri AS s_uri
                            ON e.subj_origin = s_uri.id
                        LEFT JOIN storage
                            ON e.subj_storage = storage.id
                        LEFT JOIN uri AS origin_uri
                            ON e.origin = origin_uri.id
                        LEFT JOIN uri AS s_current_uri
                            ON e.subj_id_current = s_current_uri.id
                        LEFT JOIN uri AS o_current_uri
                            ON e.subj_origin_current = o_current_uri.id
                        
                        -- WHERE uri.value != s_current_uri.value
                        ;
                        """):
    valuelist = []
    id_no = row[0] # Lfd. Nr.
    time_from_event = row[1] # Zeitpunkt
    interpretation = row[2] # Art der Aktion
    if interpretation is not None:
        interpretation = interpretation.split("#")[-1]
    manifestation = row[3] # Auslöser
    if manifestation is not None:
        manifestation = manifestation.split("#")[-1]
    application = row[4] # Anwendung
    payload = row[5] # Payload
    path = row[6] # Datei-/Programmname mit Pfad
    reason = row[7] # Typ
    if reason is not None:
        reason = reason.split("#")[-1]
    details = row[8] # Details zum Typ
    if details is not None:
        details = details.split("#")[-1]
    mimetype = row[9] # Mimetype
    subj_origin = row[10] # Übergeordneter Pfad
    text = row[11] # Programm-/ Datei- / Pfadname
    storage_value = row[12] # Speicherwert
    origin = row[13] # Ursprung
    s_current = row[14]
    o_current = row[15]


    # append values to temporary list
    valuelist.append(id_no)
    valuelist.append(time_from_event)
    valuelist.append(interpretation)
    valuelist.append(manifestation)
    valuelist.append(application)
    valuelist.append(payload)
    valuelist.append(path)
    valuelist.append(reason)
    valuelist.append(details)
    valuelist.append(mimetype)
    valuelist.append(subj_origin)
    valuelist.append(text)
    valuelist.append(storage_value)
    valuelist.append(origin)
    valuelist.append(s_current)
    valuelist.append(o_current)

    mywriter.writerow(valuelist)

target.close()


# for row in cur.execute("""
#                         SELECT e.id,
#                         datetime((e.timestamp/1000), 'unixepoch', 'localtime'),
#                         manifest.value,
#                         inter.value,
#                         s_inter.value,
#                         s_manifest.value,
#                         actor.value,
#                         mime.value,
#                         text.value,
#                         uri.value
#
#                         FROM event AS e,
#                         interpretation AS inter,
#                         manifestation AS manifest,
#                         actor,
#                         mimetype AS mime,
#                         text,
#                         uri,
#                         interpretation AS s_inter,
#                         manifestation AS s_manifest
#
#                         WHERE
#                             e.interpretation = inter.id AND
#                             e.manifestation = manifest.id AND
#                             e.actor = actor.id AND
#                             e.subj_mimetype = mime.id AND
#                             e.subj_text = text.id AND
#                             e.subj_id = uri.id AND
#                             e.subj_interpretation = s_inter.id AND
#                             e.subj_manifestation = s_manifest.id AND
#                             e.id < 101;
#                         """):