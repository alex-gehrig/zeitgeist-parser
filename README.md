# Introduction
Recently I stumbled upon the daemon "zeitgeist", which is automatically enabled
when you use Ubuntu or other distributions relying on Ubuntu. It collects many
data about what you did with your computer and so I found it interesting to play
around with the data collected by "zeitgeist".

# Location of the data
"zeitgeist" stores the collected data in a SQLite-Database, which can be found in
the home-folder of the correspondent user: `~/.local/share/zeitgeist`

The name of the database ist *activity.sqlite*

# Looking at the data
To have a look at the data collected by "zeitgeist" you can install and use
the zeitgeist-explorer: `sudo apt install zeitgeist-explorer`

This is pretty nice, but besides "filtering" the results based on the used
programs, there is not much more you can do.

The other option is of course looking into the database itself and writing a script
that parses the data and creates a more user-friendly output. This was the birth of
this project and I started writing on my little Python-script.

At the first moment, the database respectively it's structure is pretty clear.
You have a "main" table called *event*. This contains an *id* and a *timestamp*.
According to the zeitgeist-project the *timestamp* is in __gint64__ which I never
heard before. It is pretty like the well known unix-timestamp. The difference
is: it counts milliseconds and not seconds. So it is enough to divide the value
of the column *timestamp* by 1000 for further usage.

The foreign keys in the table event are only partly self-explaining. But fortunately
the guys from the "zeigeist"-project took care of referencial integrity and so you
can find a set of instructions dealing with that. To illustrate this, look at the
following screenshot, taken after opening my activity.sqlite with the "DB Browser
for SQLite".

![Screenshot](zeitgeist_sql.png)

You can see for example, that the value in the colum *payload* of the table *event*
is a foreign key to the column *id* in the table *payload*. With this information
given, it is not that difficult to write the code to parse the db and to create
an output. I decided to create a csv-file out of the data, since you then have many
possibilities like sorting, filtering, etc.

# Usage
Put the file *zeitgeist-parser.py* in the same directory as the *activity.sqlite*.
Although I have no real concerns, because the script only __reads__ from the
databse, I strongly recommend to use a copy of the file instead of using the
original one. After that just type

`python3 zeitgeist-parser activity.sqlite`

to execute the script. You will not see any status-messages or something like
that while the script works. After work is finished, you will find a csv-file
which contains the timestamp of when the script was used.